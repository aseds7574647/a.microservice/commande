const express = require('express');
const router = express.Router();

const commandeController = require('../controllers/stuff');

// Routes liées aux commandes
router.post('/', commandeController.ajouterCommande);
router.put('/:id', commandeController.updateCommande);

module.exports = router;