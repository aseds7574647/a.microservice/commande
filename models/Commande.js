const mongoose = require('mongoose');

const commandeSchema = new mongoose.Schema({
    productId: { type: String },
    title: { type: String },
    dateCommande: { type: Date, default: Date.now },
    quantite: { type: Number, default: 1 },
    commandePayee: { type: Boolean, default: false },
});

module.exports = mongoose.model('Commande', commandeSchema);
