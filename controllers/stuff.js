const Commande = require('../models/Commande');

exports.ajouterCommande = (req, res) => {
    const nouvelleCommande = new Commande({
        productId: req.body.productId,
        title: req.body.title,
    });

    nouvelleCommande.save().then(
        (command) => {
            res.status(201).json(command);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

// Mettre à jour une commande
exports.updateCommande = (req, res) => {
    const updatedCommande = new Commande({
        _id: req.params.id,
        commandePayee: req.body.commandePayee
    });
    Commande.updateOne({_id: req.params.id}, updatedCommande).then(
        (product) => {
            res.status(201).json({
                message: 'Commande updated successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};
